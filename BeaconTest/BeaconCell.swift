//
//  BeaconCell.swift
//  BeaconTest
//
//  Created by Theodore Kolinko on 01/06/2020.
//  Copyright © Theodore Kolinko. 2020 All rights reserved.
//

import UIKit

class BeaconCell: UITableViewCell {

	@IBOutlet weak var idLabel: UILabel!
	@IBOutlet weak var majorLabel: UILabel!
	@IBOutlet weak var minorLabel: UILabel!
	@IBOutlet weak var proximityLabel: UILabel!
	@IBOutlet weak var unknown: UIView!
	@IBOutlet weak var far: UIView!
	@IBOutlet weak var near: UIView!
	@IBOutlet weak var immediate: UIView!
	
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
