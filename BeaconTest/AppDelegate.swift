//
//  AppDelegate.swift
//  BeaconTest
//
//  Created by Theodore Kolinko on 01/06/2020.
//  Copyright © Theodore Kolinko. 2020 All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
	var window: UIWindow?

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		return true
	}
}

